@extends('layout.master')

@section('judul')
Detail Mahasiswa
@endsection

@section('content')
<h1>{{$mahasiswa->nama_mahasiswa}}</h1>
<p>{{$mahasiswa->jenis_kelamin}}</p>
<p>{{$mahasiswa->notelp}}</p>
<p>{{$mahasiswa->alamat}}</p>
@endsection