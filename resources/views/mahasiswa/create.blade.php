@extends('layout.master')

@section('judul')
Tambah Mata Kuliah
@endsection

@section('content')
<form action="/mahasiswa" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Mahasiswa</label>
        <input type="text" name="nama_mahasiswa" class="form-control">
    </div>
    @error('nama_mahasiswa')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Jenis Kelamin</label>
        <input type="text" name="jenis_kelamin" class="form-control">
    </div>
    @error('jenis_kelamin')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Npmor Telepon</label>
        <input type="text" name="notelp" class="form-control">
    </div>
    @error('notelp')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control"></textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection