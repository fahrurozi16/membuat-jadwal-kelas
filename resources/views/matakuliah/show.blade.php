@extends('layout.master')
@section('title')
Halaman Detail Matakuliah
@endsection
@section('content')
<a href="/matakuliah" class="btn btn-primary mb-3">Kembali</a>
<h1>{{$matakuliah->nama_mata_kuliah}}</h1>
<p>{{$matakuliah->sks}}</p>

@endsection
