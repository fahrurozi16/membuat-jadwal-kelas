@extends('layout.master')
@section('title')
Halaman Entri Matakuliah
@endsection
@section('content')



<form method="POST" action="/matakuliah">
    @csrf
  <div class="form-group">
    <label>Nama Matakuliah</label>
    <input type="text" name="nama_mata_kuliah" class="form-control">
  </div>
  @error('nama_mata_kuliah')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>SKS</label>
    <input type="text" name="sks" class="form-control">
  </div>
  @error('sks')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection

