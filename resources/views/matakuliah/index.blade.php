@extends('layout.master')
@section('title')
Halaman List Matakuliah
@endsection
@section('content')

<a href="/matakuliah/create" class="btn btn-primary mb-3">Tambah Matakuliah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Matakuliah</th>
      <th scope="col">SKS</th>
    
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
   @forelse ($matakuliah as $key => $item)
   <tr>
       <td>{{$key + 1}}</td>
       <td>{{$item->nama_mata_kuliah}}</td>
       <td>{{$item->sks}}</td>
       
       <td>
           

           <form class="mt-2" action="matakuliah/{{$item->id}}" method="POST">
           <a href="/matakuliah/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
           <a href="/matakuliah/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
               @csrf
               @method('delete')
               <input type="submit" value="Delete" class="btn btn-danger btn-sm">

           </form>
       </td>
   </tr>

   @empty
    <h1>Data tidak Ada</h1>

   @endforelse
  </tbody>
</table>

@endsection