@extends('layout.master')
@section('title')
Halaman List Jurusan
@endsection
@section('content')

<a href="/jurusan/create" class="btn btn-secondary mb-3">Tambah Jurusan</a>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Jurusan</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($jurusan as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama_jurusan}}</td>
                <td>
                    <form class="mt-2" action="jurusan/{{$item->id}}" method="POST">
                        <a href="/jurusan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/jurusan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
</table>
@endsection