@extends('layout.master')
@section('title')
Halaman Edit Jurusan
@endsection
@section('content')

<form method="POST" action="/jurusan/{{$jurusan->id}}">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Nama Jurusan</label>
    <input type="text" name="nama_jurusan"  value="{{$jurusan->nama_jurusan}}" class="form-control">
  </div>
  @error('nama_jurusan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection