@extends('layout.master')

@section('judul')
Edit Identitas Dosen
@endsection

@section('content')
<form action="/dosen/{{$dosen->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama Dosen</label>
        <input type="text" name="nama_dosen" class="form-control">
    </div>
    @error('nama_dosen')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Nomor Telepon</label>
        <input type="number" name="notelp" class="form-control">
    </div>
    @error('notelp')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="alamat" class="form-control"></textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection