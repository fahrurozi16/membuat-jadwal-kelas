@extends('layout.master')

@section('judul')
List Dosen
@endsection

@section('content')
<a href="/dosen/create" class="btn btn-secondary mb-3">Tambah Dosen</a>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Dosen</th>
            <th scope="col">Nomor Telepon</th>
            <th scope="col">Alamat</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($dosen as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama_dosen}}</td>
                <td>{{$item->notelp}}</td>
                <td>{{$item->alamat}}</td>
                <td>
                    <form action="/dosen/{{$item->id}}" method="POST">
                        <a href="/dosen/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/dosen/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
</table>
@endsection