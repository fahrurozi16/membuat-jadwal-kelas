@extends('layout.master')

@section('judul')
Tambah Jadwal Kuliah
@endsection

@section('content')
<form action="/jadwal" method="POST">
    @csrf
    <div class="form-group">
        <label>Tanggal</label>
        <input type="date" name="tanggal" class="form-control">
    </div>
    @error('tanggal')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Ruangan</label>
        <input type="text" name="ruangan" class="form-control">
    </div>
    @error('ruangan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection