@extends('layout.master')

@section('judul')
List Dosen
@endsection

@section('content')
<a href="/jadwal/create" class="btn btn-secondary mb-3">Tambah Dosen</a>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Ruangan</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($dosen as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->tangggal}}</td>
                <td>{{$item->ruangan}}</td>
                <td>
                    <form action="/jadwal/{{$item->id}}" method="POST">
                        <a href="/jadwal/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/jadwal/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
</table>
@endsection