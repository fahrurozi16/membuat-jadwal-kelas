<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
//CRUD dosen
//Create
Route::get('/dosen/create', 'DosenController@create');
Route::post('/dosen', 'DosenController@store');
//read
Route::get('/dosen', 'DosenController@index');
Route::get('/dosen/{dosen_id}', 'DosenController@show');
//Update
Route::get('/dosen/{dosen_id}/edit', 'DosenController@edit'); 
Route::put('/dosen/{dosen_id}', 'DosenController@update');
//Delete
Route::delete('/dosen/{dosen_id}', 'DosenController@destroy');

//CRUD matakuliah
//Create
Route::get('/matakuliah/create', 'MatakuliahController@create');
Route::post('/matakuliah', 'MatakuliahController@store');
//read
Route::get('/matakuliah', 'MatakuliahController@index');
Route::get('/matakuliah/{matakuliah_id}', 'MatakuliahController@show');
//Update
Route::get('/matakuliah/{matakuliah_id}/edit', 'MatakuliahController@edit'); 
Route::put('/matakuliah/{matakuliah_id}', 'MatakuliahController@update');
//Delete
Route::delete('/matakuliah/{matakuliah_id}', 'MatakuliahController@destroy');

//CRUD jurusan
//Create
Route::get('/jurusan/create', 'JurusanController@create');
Route::post('/jurusan', 'JurusanController@store');
//read
Route::get('/jurusan', 'JurusanController@index');
Route::get('/jurusan/{jurusan_id}', 'JurusanController@show');
//Update
Route::get('/jurusan/{jurusan_id}/edit', 'JurusanController@edit'); 
Route::put('/jurusan/{jurusan_id}', 'JurusanController@update');
//Delete
Route::delete('/jurusan/{jurusan_id}', 'JurusanController@destroy');

Auth::routes();


