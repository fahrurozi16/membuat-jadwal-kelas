<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $table = 'profile';

    protected $fillable = ['nama_mahasiswa', 'jurusan_id', 'matakuliah_id', 'jenis_kelamin' ]
}
