<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matakuliah;

class MatakuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliah = Matakuliah::all();

        return view('matakuliah.index', compact('matakuliah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('matakuliah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_mata_kuliah' => 'required',
            'sks' => 'required',
            
        ],
        [
            'nama_mata_kuliah.required' => 'Nama Matakuliah harus diisi',
            'sks.required'  => 'No Telpon harus diisi',
           
        ]
    );
    
    $matakuliah = new Matakuliah;
 
    $matakuliah->nama_mata_kuliah = $request->nama_mata_kuliah;
    $matakuliah->sks = $request->sks;
    

    $matakuliah->save();
     
    return redirect('/matakuliah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matakuliah = Matakuliah::find($id);

        return view('matakuliah.show', compact('matakuliah'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matakuliah = Matakuliah::find($id);

        return view('matakuliah.edit', compact('matakuliah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_mata_kuliah' => 'required',
            'sks' => 'required',
            
        ],
        [
            'nama_mata_kuliah.required' => 'Nama Matakuliah harus diisi',
            'sks.required'  => 'SKS harus diisi',
            
        ]
    );
    $matakuliah = Matakuliah::find($id);
 
    $matakuliah->nama_mata_kuliah = $request['nama_mata_kuliah'];
    $matakuliah->sks = $request['sks'];
    
 
    $matakuliah->save();

    return redirect('/matakuliah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matakuliah = Matakuliah::find($id);

        $matakuliah->delete();

        return redirect('/matakuliah');
    }
}
