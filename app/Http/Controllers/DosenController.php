<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::all();

        return view('dosen.index', compact('dosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namadosen' => 'required',
            'notelp' => 'required',
            'alamat' => 'required|max:255',
        ],
        [
            'namadosen.required' => 'Nama Dosen harus diisi',
            'notelp.required'  => 'No Telpon harus diisi',
            'alamat.required'  => 'Alamat tidak boleh kosong',
        ]
    );
    
    $dosen = new Dosen;
 
    $dosen->namadosen = $request->namadosen;
    $dosen->notelp = $request->notelp;
    $dosen->alamat = $request->alamat;

    $dosen->save();
     
    return redirect('/dosen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dosen = dosen::find($id);

        return view('dosen.show', compact('dosen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $dosen = Dosen::find($id);

        return view('dosen.edit', compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namadosen' => 'required',
            'notelp' => 'required',
            'alamat' => 'required|max:255',
        ],
        [
            'namadosen.required' => 'Nama Dosen harus diisi',
            'notelp.required'  => 'No Telpon harus diisi',
            'alamat.required'  => 'Alamat tidak boleh kosong',
        ]
    );
    $dosen = Dosen::find($id);
 
    $dosen->namadosen = $request['namadosen'];
    $dosen->notelp = $request['notelp'];
    $dosen->alamat = $request['alamat'];
 
    $dosen->save();

    return redirect('/dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dosen = Dosen::find($id);

        $dosen->delete();

        return redirect('/dosen');
    }
}
